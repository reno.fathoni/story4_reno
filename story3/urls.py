from django.urls import path
from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.ind, name='ind'),
    path('skill/', views.skill, name='skill'),
    path('experiance/', views.experiance, name='experiance'),
    path('contact/', views.contact, name='contact'),
    path('gallery/', views.gallery, name='gallery'),
    # dilanjutkan ...
]